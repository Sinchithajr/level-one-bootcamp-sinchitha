//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float S1(float x1, float x2)
{
return x2-x1;
}
float S2(float y1, float y2)
{
return y2-y1;
}
float distance(float x1,float x2, float y1, float y2)
{
return sqrt(S1(x1,x2)*S1(x1,x2)+S2(y1,y2)*S2(y1,y2));
}
int main()
{
float x1,x2,y1,y2,dis;
printf("Enter the co-ordinate of the first point (x1,y1): \n");
scanf("%f%f",&x1,&y1);
printf("Enter the co-ordinate of the second point (x2,y2): \n");
scanf("%f%f",&x2,&y2);
dis=distance(x1,x2,y1,y2);
printf("Distance between(%.2f, %.2f) and (%.2f, %.2f)is %.2f",x1,y1,x2,y2,dis);
return 0;
}

